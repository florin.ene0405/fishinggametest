using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public void LoadNewScene(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            return;
        }

        SaveState();

        SceneManager.LoadScene(name);
    }

    public void SaveState()
    {
        PlayerPrefs.SetInt(Consts.IsMultyplayerkey, 0);
    }
}
