using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class GameController : MonoBehaviour
{
    public Transform BoatSpawnPoint;
    public List<FishSpawnPoint> FishSpawnPoint = new List<FishSpawnPoint>();
    public BoatController BoatPrefab;
    public FishController FishPrefab;
    public BoatScriptableObject BoatSO;
    public FishScriptableObject FishSO;

    public GameObject TextHolder;
    public TextMeshProUGUI NormalCounter;
    public TextMeshProUGUI MagicCounter;
    public TextMeshProUGUI RareCounter;
    public TextMeshProUGUI UniqueCounter;

    public Button PauseButton;

    public Canvas Canvas;

    private int _normalFishCounter;
    private int _normalFishCought;
    private int _magicFishCounter;
    private int _magicFishCought;
    private int _rareFishCounter;
    private int _rareFishCought;
    private int _uniqueFishCounter;
    private int _uniqueFishCought;

    private BoatController _boat;

    public void Start()
    {
        var isMulti = CheckIfMultiplayer();

        if (isMulti)
        {
            var boat = PhotonNetwork.Instantiate(BoatPrefab.name, BoatSpawnPoint.position, Quaternion.identity);
            _boat = boat.GetComponent<BoatController>();
            _boat.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
        }
        else
        {
            _boat = Instantiate<BoatController>(BoatPrefab, BoatSpawnPoint);
        }


        _boat.Initialize(BoatSO.Speed, BoatSO.HookSpeed, isMulti, this);
        StartCoroutine(InitialFishesSpawn());
        UpdateUI();
        TextHolder.SetActive(true);
        PauseButton.gameObject.SetActive(true);
        PauseButton.gameObject.SetActive(true);
    }

    public bool CheckIfMultiplayer() => PlayerPrefs.GetInt(Consts.IsMultyplayerkey) == 1;


    private Vector2 GetRandomPosint(FishSpawnPoint fishController)
    {
        Bounds bounds = fishController.transform.GetComponent<BoxCollider2D>().bounds;

        return bounds.center + new Vector3(
           (Random.value - 0.5f) * bounds.size.x,
           (Random.value - 0.5f) * bounds.size.y
        );
    }

    private IEnumerator InitialFishesSpawn()
    {
        for (int i = 0; i < FishSO.StartFishes; i++)
        {
            yield return new WaitForSeconds(0.3f);
            SpawnAFish();
        }
    }

    private void SpawnAFish(FishController fish = null)
    {
        int locationIndex = Random.Range(0, FishSpawnPoint.Count);
        int fishIndex = Random.Range(0, FishSO.Fish.Count);
        FishSpawnPoint myFishSP = FishSpawnPoint[locationIndex];

        FishController newFish;

        if (fish == null)
        {
            newFish = Instantiate<FishController>(FishPrefab, GetRandomPosint(myFishSP), Quaternion.identity, myFishSP.transform);
        }
        else
        {
            newFish = fish;
            newFish.transform.position = GetRandomPosint(myFishSP);
        }

        newFish.transform.localScale = Vector2.one;

        newFish.Initialized(FishSpawnPoint[locationIndex].Direction, FishSO.Fish[fishIndex].Sprite, FishSO.Fish[fishIndex].Speed, FishSO.Fish[fishIndex].CaughtPercentage, FishSO.Fish[fishIndex].FishRarity, this);
    }

    public void FishCaught(FishController fish)
    {
        CountFishes(fish, true);
        SpawnAFish(fish);
        _boat.ShowAnimation(BoatAnimation.Cought);
    }

    public void FailToCoughtFish(FishController fish)
    {
        CountFishes(fish, false);
        _boat.ShowAnimation(BoatAnimation.Fail);
    }

    public void TryToCoughtFish()
    {
        _boat.ShowAnimation(BoatAnimation.Trow);
    }

    private void CountFishes(FishController fish, bool cought)
    {
        switch (fish.FishRarity)
        {
            case FishRarity.None:
                break;
            case FishRarity.Normal:

                _normalFishCounter++;

                if (cought)
                {
                    _normalFishCought++;
                }

                break;
            case FishRarity.Magic:

                _magicFishCounter++;

                if (cought)
                {
                    _magicFishCought++;
                }

                break;
            case FishRarity.Rare:

                _rareFishCounter++;

                if (cought)
                {
                    _rareFishCought++;
                }

                break;
            case FishRarity.Unique:

                _uniqueFishCounter++;

                if (cought)
                {
                    _uniqueFishCought++;
                }

                break;
            default:
                break;
        }
        UpdateUI();
    }

    private void UpdateUI()
    {
        if (NormalCounter == null || MagicCounter == null || RareCounter == null || UniqueCounter == null)
        {
            Debug.LogError("A counter is missing");
            return;
        }

        NormalCounter.text = $"{_normalFishCought} / {_normalFishCounter}";
        MagicCounter.text = $"{_magicFishCought} / {_magicFishCounter}";
        RareCounter.text = $"{_rareFishCought} / {_rareFishCounter}";
        UniqueCounter.text = $"{_uniqueFishCought} / {_uniqueFishCounter}";
    }
}

public enum Direction
{
    None = 0,
    Right = 1,
    Left = 2
}
