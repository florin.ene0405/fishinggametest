using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoatScriptableObject", menuName = "ScriptableObjects/BoatScriptableObject")]

public class BoatScriptableObject : ScriptableObject
{
    public int Speed;
    public int HookSpeed;
}
