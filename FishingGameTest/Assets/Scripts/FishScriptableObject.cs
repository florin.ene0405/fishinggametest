using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FishScriptableObject", menuName = "ScriptableObjects/FishScriptableObject")]
public class FishScriptableObject : ScriptableObject
{
    public int StartFishes;
    public List<FishData> Fish = new List<FishData>();
}

[System.Serializable]
public class FishData
{
    public FishRarity FishRarity;
    public Sprite Sprite;
    public int Speed;
    public int CaughtPercentage;
}

public enum FishRarity
{
    None = 0,
    Normal = 1,
    Magic = 2,
    Rare = 3,
    Unique = 4
}
