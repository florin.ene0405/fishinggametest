using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Consts 
{
    public static readonly string IsMultyplayerkey = "IS-MULTIPLAYER";
    public static readonly string GameScene = "GameScene";
    public static readonly string MenuScene = "MenuScene";
    public static readonly string LobbyScene = "LobbyScene";
}
