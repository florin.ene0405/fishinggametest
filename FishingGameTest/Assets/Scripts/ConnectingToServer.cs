using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ConnectingToServer : MonoBehaviourPunCallbacks
{
    public Transform ConnectIcon;
    public GameObject MultiplayerHolder;
    public GameObject ButtonHolder;


    public  void TryToConnect()
    {
        PhotonNetwork.ConnectUsingSettings();
        ConnectIcon.DORotate(new Vector3(0, 0, -180), 5f).SetEase(Ease.Linear).SetLoops(-1);
    }

    public void OpenScreen()
    {
        MultiplayerHolder.SetActive(true);
        ButtonHolder.SetActive(false);
        TryToConnect();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        ConnectIcon.DOKill();
        PhotonNetwork.JoinLobby();
    }


    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        SceneManager.LoadScene(Consts.LobbyScene);
    }
}
