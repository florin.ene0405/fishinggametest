using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public void PauseTheGame()
    {
        gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void UnPauseTheGame()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void GoBackToStart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
