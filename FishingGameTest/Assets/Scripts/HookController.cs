using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookController : MonoBehaviour
{
    private int _speed;
    private HookDirection _hookDirection = HookDirection.Down;
    private bool _trowHook = false;
    private bool _canCought = true;
    private BoatController _boatController;

    public void Initialized(int speed,BoatController boatController)
    {
        _speed = speed;
        _boatController = boatController;
    }

    public void TrowHook()
    {
        _trowHook = true;
    }

    private void Update()
    {
        if (!_trowHook)
        {
            return;
        }

        if (_hookDirection == HookDirection.Up)
        {
            transform.Translate(Vector3.up * _speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.up * _speed * Time.deltaTime * -1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Fish" && _canCought)
        {
            var fish = collision.transform.GetComponent<FishController>();
            fish.TryHook(this);
            _canCought = false;
            _trowHook = false;
            return;
        }

        if (collision.transform.tag != "HookTrigger")
        {
            return;
        }

        if (_hookDirection == HookDirection.Up)
        {
            if (_boatController == null)
            {
                return;
            }

            _boatController.ResetHook();
            _hookDirection = HookDirection.Down;
            _trowHook = false;
            _canCought = true;
        }
        else
        {
            _hookDirection = HookDirection.Up;
        }
    }

    public void RiseTHeHook()
    {
        _hookDirection = HookDirection.Up;
        _canCought = false;
        _trowHook = true;
    }

    private enum HookDirection
    {
        None = 0,
        Up = 1,
        Down = 2
    }
}
