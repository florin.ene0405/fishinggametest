using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Photon.Pun;

public class BoatController : MonoBehaviour
{
    public RectTransform Mask;
    public HookController HookController;
    public GameObject TrowHookIcon;
    public GameObject CoughtIcon;
    public GameObject FailIcon;

    private Direction _direction = Direction.Right;
    private bool _isFishing = false;
    private int _speed;
    private int _hookSpeed;
    private GameController _gameController;
    private PhotonView _photonView;
    private bool _isMultiplayer;


    private void Start()
    {
        HookController.Initialized(_hookSpeed, this);
        _photonView = GetComponent<PhotonView>();
    }

    void Update()
    {
        if (_isMultiplayer && !_photonView.IsMine)
        {
            return;
        }

        if (_isFishing)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            _isFishing = true;
            HookController.TrowHook();
        }

        if (_direction == Direction.Right)
        {
            transform.Translate(Vector3.right * _speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.right * _speed * Time.deltaTime * -1);
        }
    }

    public void ResetHook()
    {
        _isFishing = false;
        ResetIcons();
    }

    internal void Initialize(int speed, int hookSpeed, bool multy,GameController gameController)
    {
        _speed = speed;
        _hookSpeed = hookSpeed;
        _isMultiplayer = multy;
        _gameController = gameController;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag != "BoatTrigger")
        {
            return;
        }

        if (_direction == Direction.Right)
        {
            _direction = Direction.Left;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            Mask.localScale = new Vector2(-Mask.localScale.x, Mask.localScale.y);
        }
        else
        {
            _direction = Direction.Right;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
            Mask.localScale = new Vector2(-Mask.localScale.x, Mask.localScale.y);
        }
    }

    private void ResetIcons()
    {
        TrowHookIcon.SetActive(false);
        CoughtIcon.SetActive(false);
        FailIcon.SetActive(false);
    }

    public void ShowAnimation(BoatAnimation boatAnimation)
    {
        ResetIcons();

        switch (boatAnimation)
        {
            case BoatAnimation.None:
                break;
            case BoatAnimation.Trow:
                TrowHookIcon.SetActive(true);
                break;
            case BoatAnimation.Cought:
                CoughtIcon.SetActive(true);
                break;
            case BoatAnimation.Fail:
                FailIcon.SetActive(true);
                break;
            default:
                break;
        }
    }
}

public enum BoatAnimation
{
    None = 0,
    Trow = 1,
    Cought = 2,
    Fail = 3
}
