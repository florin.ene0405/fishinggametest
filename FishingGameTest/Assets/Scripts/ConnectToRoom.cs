using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;


public class ConnectToRoom : MonoBehaviourPunCallbacks
{
    public TMP_InputField CreateInput;
    public TMP_InputField JoinInput;


    public void CreatRoom()
    {
        PhotonNetwork.CreateRoom(CreateInput.text);
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(JoinInput.text);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        PlayerPrefs.SetInt(Consts.IsMultyplayerkey, 1);
        PhotonNetwork.LoadLevel(Consts.GameScene);
    }
}
