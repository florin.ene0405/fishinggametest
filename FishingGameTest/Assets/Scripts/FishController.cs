using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class FishController : MonoBehaviour
{
    private Direction _direction;
    private int _speed;
    private int _caughtPercentage;
    private bool _isCought = false;
    private GameController _gameController;
    private Animator _animator;

    public FishRarity FishRarity;

    public void Initialized(Direction direction, Sprite sprite, int speed, int caughtPercentage, FishRarity fishRarity, GameController gameController)
    {
        _direction = direction;
        _speed = speed;
        _caughtPercentage = caughtPercentage;
        GetComponent<Image>().sprite = sprite;
        _gameController = gameController;
        _isCought = false;
        FishRarity = fishRarity;

        if (_direction == Direction.Right)
        {
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
        else
        {
            transform.localScale = new Vector2(transform.localScale.x, transform.localScale.y);
        }
    }

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (_isCought)
        {
            return;
        }

        if (_direction == Direction.Right)
        {
            transform.Translate(Vector3.right * _speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.right * _speed * Time.deltaTime * -1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        FishSpawnPoint newFP = collision.transform.GetComponent<FishSpawnPoint>();

        if (newFP == null)
        {
            return;
        }

        _direction = newFP.Direction;

        if (_direction == Direction.Right)
        {
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
        else
        {
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
        }
    }

    public async void TryHook(HookController hookController)
    {
        _isCought = true;
        int number = Random.Range(0, 100);

        _animator.Play("FishAnimation");
        _gameController.TryToCoughtFish();

        await Task.Delay(700);

        if (number <= _caughtPercentage)
        {
            ResetPosition();
            _gameController.FishCaught(this);
        }
        else
        {
            _gameController.FailToCoughtFish(this);
            _isCought = false;
        }

        _animator.Play("IdleFishAnimation");

        hookController.RiseTHeHook();
    }

    private void ResetPosition()
    {
        if (transform.localScale.x < 0)
        {
            transform.localScale = Vector2.one;
        }
    }
}
